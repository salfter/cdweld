Capacitive Discharge Welder
===========================

This is a slight variation of the circuit described at
http://www.philpem.me.uk/elec/welder/.  The main difference is that I
replaced the SPDT relay with a DPDT relay to ensure that the power supply is
never connected to the capacitor bank during a weld.  This simplifies the
electronics somewhat.

It should be possible to power this from a 24V DC wall wart or brick of
maybe 2-3 amps (the most the L200C regulator can pass is 2A).  

A 12-position connector strip is provided.  A footswitch to trigger the
welder is connected to pins 1 and 2.  A 10k potentiometer is connected to
pins 3 and 4 to adjust the charging voltage.  Pins 5-9 accommodate digital
and analog panel meters to measure charging voltage and current; place a
jumper across pins 8 and 9 if current monitoring isn't needed.  Pins 10 (+)
and 12 (-) charge the capacitor bank.  Pin 11 triggers a 50RIA20 SCR to
discharge the capacitor bank into the welding terminals; connect it to the
gate.  The 50RIA20's anode is connected to the positive side of the
capacitor bank; the cathode is connected to a welding terminal (heavy-gauge
copper wire ground to a small flat).  The other welding terminal is
connected to the negative side of the capacitor bank.

The original author's capacitor bank was a group of five 0.12 F (not a typo)
surplus "computer-grade" 25V electrolytic capacitors.  These will be by far
the most expensive part to procure, especially if you can't find surplus
parts somewhere.  Digi-Key, for instance, has 0.1 F 35V capacitors starting
at about $23 each.  A single 0.68 F 25V capacitor, if you were to go that
route, starts at $148.  1 F (and larger) capacitors for car-audio use are
available at lower cost, but since they're intended for 12V use, you would
most likely need to connect them in series.

