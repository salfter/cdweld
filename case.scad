//board();

bottom();

translate([-5,0,21])
rotate([0,180,0])
	top();

module square_peg()
{
	rotate([90,0,-90])
	difference()
	{
		translate([0,6,-2.7])
		linear_extrude(5.4, convexity=10)
			polygon([[-2.7,-6],[2.7,-6],[2.7,8],[0,8]]);
		
		translate([0,6,0])
		rotate([90,0,0])
		linear_extrude(6, convexity=10)
			circle(d=3, $fn=45);

	}
}

module board() // board model
{
	color("gray")
	translate([4,4,4.5])
		import("cdweld.stl");
}
	
module bottom() // case bottom
{
	difference()
	{
		union()
		{
			linear_extrude(2, convexity=10)
				polygon([[0,0],[70,0],[70,57.5],[0,57.5]]);

			translate([0,40.5,2])
			rotate([90,0,90])
			linear_extrude(2, convexity=10)
				polygon([[0,0],[15,0],[15,10],[0,10]]);
			
			translate([68,40.5,2])
			rotate([90,0,90])
			linear_extrude(2, convexity=10)
				polygon([[0,0],[15,0],[15,10],[0,10]]);
			
			translate([0,57.5,2])
			rotate([90,0,0])
			linear_extrude(2, convexity=10)
				polygon([[0,0],[70,0],[70,10],[0,10]]);

			translate([8.1,7.2,2])
			linear_extrude(1.6, convexity=10)
			difference()
			{
				circle(d=5.4, $fn=45);
				circle(d=3.4, $fn=45);
			}

			translate([63.4,7.2,2])
			linear_extrude(1.6, convexity=10)
			difference()
			{
				circle(d=5.4, $fn=45);
				circle(d=3.4, $fn=45);
			}

			translate([8.1,37.2,2])
			linear_extrude(1.6, convexity=10)
			difference()
			{
				circle(d=5.4, $fn=45);
				circle(d=3.4, $fn=45);
			}

			translate([63.4,37.2,2])
			linear_extrude(1.6, convexity=10)
			difference()
			{
				circle(d=5.4, $fn=45);
				circle(d=3.4, $fn=45);
			}

		}
		
		translate([8.1,7.2,0])
		linear_extrude(2, convexity=10)
			circle(d=3.4, $fn=45);
		
		translate([63.4,7.2,0])
		linear_extrude(2, convexity=10)
			circle(d=3.4, $fn=45);
		
		translate([8.1,37.2,0])
		linear_extrude(2, convexity=10)
			circle(d=3.4, $fn=45);
		
		translate([63.4,37.2,0])
		linear_extrude(2, convexity=10)
			circle(d=3.4, $fn=45);
	}
}

module top() // case top
{
	translate([0,0,19])
	linear_extrude(2, convexity=10)
	difference()
	{
		polygon([[0,0],[70,0],[70,35.5],[0,35.5]]);
		polygon([[45.5,5],[58,5],[58,11.5],[45.5,11.5]]);
	}
	
	translate([8.1,7.2,5.5])
	linear_extrude(14, convexity=10)
	difference()
	{
		circle(d=5.4, $fn=45);
		circle(d=3, $fn=45);
	}
	
	translate([63.4,7.2,5.5])
	linear_extrude(14, convexity=10)
	difference()
	{
		circle(d=5.4, $fn=45);
		circle(d=3, $fn=45);
	}
	
	translate([8.1,37.2,5.5])
		square_peg();
	
	translate([63.4,37.2,5.5])
		square_peg();
		
	translate([0,2,2])
	rotate([90,0,0])
	linear_extrude(2, convexity=10)
		polygon([[0,0],[70,0],[70,19],[0,19]]);
		
	translate([0,0,2])
	rotate([90,0,90])
	linear_extrude(2, convexity=10)
	difference()
	{
		polygon([[0,0],[40.4,0],[40.4,10],[35.5,19],[0,19]]);
		translate([15.5,9.5])
			circle(d=9, $fn=45);
	}
	
	translate([68,0,2])
	rotate([90,0,90])
	linear_extrude(2, convexity=10)
		polygon([[0,0],[40.4,0],[40.4,10],[35.5,19],[0,19]]);
	
}

