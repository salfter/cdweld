EESchema Schematic File Version 2
LIBS:proper-passives
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:cdweld-cache
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L L200 U1
U 1 1 5A3D7B05
P 4100 3900
F 0 "U1" H 3850 4125 50  0000 C CNN
F 1 "L200C" H 4300 4125 50  0000 C CNN
F 2 "TO_SOT_Packages_THT:TO-220-5_Vertical_StaggeredType1" H 4100 4225 50  0001 C CNN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/04/2c/9f/b5/65/15/49/ac/CD00000053.pdf/files/CD00000053.pdf/jcr:content/translations/en.CD00000053.pdf" H 4100 4100 50  0001 C CNN
F 4 "497-1382-5-ND" H 4100 3900 60  0001 C CNN "Vendor_PN"
F 5 "3.29" H 4100 3900 60  0001 C CNN "Price"
F 6 "Digi-Key" H 4100 3900 60  0001 C CNN "Vendor"
	1    4100 3900
	1    0    0    -1  
$EndComp
$Comp
L CP C1
U 1 1 5A3D7BD4
P 3200 4000
F 0 "C1" H 3225 4100 50  0000 L CNN
F 1 "10u" H 3225 3900 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D5.0mm_P2.00mm" H 3200 4000 50  0001 C CNN
F 3 "https://search.kemet.com/component-edge/download/datasheet/ESK106M035AC3AA.pdf" H 3200 4000 50  0001 C CNN
F 4 "Digi-Key" H 3200 4000 60  0001 C CNN "Vendor"
F 5 "399-6598-ND" H 3200 4000 60  0001 C CNN "Vendor_PN"
F 6 ".15" H 3200 4000 60  0001 C CNN "Price"
	1    3200 4000
	1    0    0    -1  
$EndComp
$Comp
L Barrel_Jack J1
U 1 1 5A3D7BFF
P 2550 3700
F 0 "J1" H 2550 3910 50  0000 C CNN
F 1 "24V In" H 2550 3525 50  0000 C CNN
F 2 "Connect:Barrel_Jack_CUI_PJ-102AH" H 2600 3660 50  0001 C CNN
F 3 "http://www.cui.com/product/resource/digikeypdf/pj-102ah.pdf" H 2600 3660 50  0001 C CNN
F 4 "CP-102AH-ND" H 2550 3700 60  0001 C CNN "Vendor_PN"
F 5 ".76" H 2550 3700 60  0001 C CNN "Price"
F 6 "Digi-Key" H 2550 3700 60  0001 C CNN "Vendor"
	1    2550 3700
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5A3D7C82
P 4300 4300
F 0 "R1" V 4380 4300 50  0000 C CNN
F 1 "1k" V 4200 4300 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4230 4300 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-CF_CFM.pdf" H 4300 4300 50  0001 C CNN
F 4 "CF14JT1K00CT-ND" V 4300 4300 60  0001 C CNN "Vendor_PN"
F 5 ".10" V 4300 4300 60  0001 C CNN "Price"
F 6 "Digi-Key" V 4300 4300 60  0001 C CNN "Vendor"
	1    4300 4300
	0    -1   -1   0   
$EndComp
$Comp
L GNDREF #PWR01
U 1 1 5A3D7DD1
P 4100 4550
F 0 "#PWR01" H 4100 4300 50  0001 C CNN
F 1 "GNDREF" H 4100 4400 50  0000 C CNN
F 2 "" H 4100 4550 50  0001 C CNN
F 3 "" H 4100 4550 50  0001 C CNN
	1    4100 4550
	1    0    0    -1  
$EndComp
$Comp
L GNDREF #PWR02
U 1 1 5A3D948C
P 5950 4950
F 0 "#PWR02" H 5950 4700 50  0001 C CNN
F 1 "GNDREF" H 5950 4800 50  0000 C CNN
F 2 "" H 5950 4950 50  0001 C CNN
F 3 "" H 5950 4950 50  0001 C CNN
	1    5950 4950
	1    0    0    -1  
$EndComp
$Comp
L D_ALT D1
U 1 1 5A3D97C9
P 6150 4650
F 0 "D1" H 6150 4750 50  0000 C CNN
F 1 "1N4001" H 6150 4550 50  0000 C CNN
F 2 "Diodes_ThroughHole:D_DO-41_SOD81_P10.16mm_Horizontal" H 6150 4650 50  0001 C CNN
F 3 "http://www.mccsemi.com/up_pdf/1N4001-1N4007(DO-41).pdf" H 6150 4650 50  0001 C CNN
F 4 "Digi-Key" H 6150 4650 60  0001 C CNN "Vendor"
F 5 "1N4001-TPMSCT-ND" H 6150 4650 60  0001 C CNN "Vendor_PN"
F 6 ".11" H 6150 4650 60  0001 C CNN "Price"
	1    6150 4650
	0    1    1    0   
$EndComp
$Comp
L GNDREF #PWR03
U 1 1 5A3D9F7C
P 8050 4700
F 0 "#PWR03" H 8050 4450 50  0001 C CNN
F 1 "GNDREF" H 8050 4550 50  0000 C CNN
F 2 "" H 8050 4700 50  0001 C CNN
F 3 "" H 8050 4700 50  0001 C CNN
	1    8050 4700
	1    0    0    -1  
$EndComp
Text Notes 8800 3950 0    60   ~ 0
Charge
Text Notes 8800 4050 0    60   ~ 0
SCR Trigger
Text Notes 8800 4150 0    60   ~ 0
Ground
$Comp
L C C2
U 1 1 5A3DBCC9
P 3500 4000
F 0 "C2" H 3525 4100 50  0000 L CNN
F 1 "0.1u" H 3525 3900 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 3500 4000 50  0001 C CNN
F 3 "http://www.vishay.com/docs/45171/kseries.pdf" H 3500 4000 50  0001 C CNN
F 4 "BC2665CT-ND" H 3500 4000 60  0001 C CNN "Vendor_PN"
F 5 ".18" H 3500 4000 60  0001 C CNN "Price"
F 6 "Digi-Key" H 3500 4000 60  0001 C CNN "Vendor"
	1    3500 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 3800 3700 3800
Wire Wire Line
	3500 3850 3500 3800
Connection ~ 3500 3800
Connection ~ 3200 3800
Wire Wire Line
	4100 4200 4100 4550
Wire Wire Line
	3000 4300 4150 4300
Wire Wire Line
	4500 4000 4650 4000
Wire Wire Line
	4550 4000 4550 4300
Connection ~ 4100 4300
Wire Wire Line
	4550 3900 4550 3800
Wire Wire Line
	4500 3800 6300 3800
Wire Wire Line
	3500 4300 3500 4150
Wire Wire Line
	3200 4300 3200 4150
Connection ~ 3500 4300
Wire Wire Line
	3000 4300 3000 3600
Wire Wire Line
	3000 3600 2850 3600
Connection ~ 3200 4300
Wire Wire Line
	5200 3800 5200 3850
Connection ~ 4550 3800
Wire Wire Line
	5200 4150 5200 4550
Connection ~ 5200 3800
Wire Wire Line
	6150 4450 6150 4500
Wire Wire Line
	6150 4800 6150 4850
Wire Wire Line
	8050 4100 8500 4100
Wire Wire Line
	8050 3600 8050 4700
Wire Wire Line
	5950 4450 6300 4450
Wire Wire Line
	3200 3800 3200 3850
Connection ~ 6150 4450
Wire Wire Line
	5950 4850 6300 4850
Wire Wire Line
	5950 4850 5950 4950
Connection ~ 6150 4850
Wire Wire Line
	6800 3700 8500 3700
Text Notes 8800 3450 0    60   ~ 0
Meter Power
Text Notes 8800 3550 0    60   ~ 0
Meter V+
Text Notes 8800 3650 0    60   ~ 0
Meter V-
Text Notes 8800 3750 0    60   ~ 0
Meter A+
Text Notes 8800 3850 0    60   ~ 0
Meter A-
$Comp
L C C5
U 1 1 5A405BFC
P 5200 4000
F 0 "C5" H 5225 4100 50  0000 L CNN
F 1 "0.1u" H 5225 3900 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 5200 4000 50  0001 C CNN
F 3 "http://www.vishay.com/docs/45171/kseries.pdf" H 5200 4000 50  0001 C CNN
F 4 "BC2665CT-ND" H 5200 4000 60  0001 C CNN "Vendor_PN"
F 5 ".18" H 5200 4000 60  0001 C CNN "Price"
F 6 "Digi-Key" H 5200 4000 60  0001 C CNN "Vendor"
	1    5200 4000
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x12 J2
U 1 1 5A405D7F
P 8700 3500
F 0 "J2" H 8700 4100 50  0000 C CNN
F 1 "Conn_01x12" H 8700 2800 50  0001 C CNN
F 2 "Connectors_Phoenix:PhoenixContact_MSTBA-G_12x5.00mm_Angled" H 8700 3500 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Phoenix%20Contact%20PDFs/1988901.pdf" H 8700 3500 50  0001 C CNN
F 4 "Digi-Key" H 8700 3500 60  0001 C CNN "Vendor"
F 5 "277-1774-ND" H 8700 3500 60  0001 C CNN "Vendor_PN"
F 6 "2.07" H 8700 3500 60  0001 C CNN "Price"
	1    8700 3500
	1    0    0    -1  
$EndComp
Text Notes 8800 3050 0    60   ~ 0
Footswitch
Text Notes 8800 3150 0    60   ~ 0
Footswitch
Text Notes 8800 3250 0    60   ~ 0
Potentiometer
Text Notes 8800 3350 0    60   ~ 0
Potentiometer
Wire Wire Line
	8500 3800 8450 3800
Wire Wire Line
	8450 3800 8450 3900
Wire Wire Line
	8450 3900 8500 3900
Wire Wire Line
	8050 3600 8500 3600
Wire Wire Line
	6100 3800 6100 3500
Wire Wire Line
	6100 3500 8500 3500
Connection ~ 6100 3800
Wire Wire Line
	8050 3000 8500 3000
Wire Wire Line
	8500 3100 5950 3100
Wire Wire Line
	5950 3100 5950 4450
Wire Wire Line
	4550 3900 4500 3900
Wire Wire Line
	8200 3200 8500 3200
Wire Wire Line
	8200 3200 8200 3500
Connection ~ 8200 3500
Wire Wire Line
	4650 3300 8500 3300
Connection ~ 8050 4100
Wire Wire Line
	4650 4000 4650 3300
Connection ~ 4550 4000
$Comp
L L78L05_TO92 U2
U 1 1 5A406BE9
P 4100 5200
F 0 "U2" H 3950 5325 50  0000 C CNN
F 1 "L78L05_TO92" H 4100 5325 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Wide" H 4100 5425 50  0001 C CIN
F 3 "http://www.taiwansemi.com/products/datasheet/TS78L00_I15.pdf" H 4100 5150 50  0001 C CNN
F 4 "Digi-Key" H 4100 5200 60  0001 C CNN "Vendor"
F 5 "TS78L05CT A3GCT-ND" H 4100 5200 60  0001 C CNN "Vendor_PN"
F 6 ".44" H 4100 5200 60  0001 C CNN "Price"
	1    4100 5200
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 5A406CC8
P 3500 5450
F 0 "C3" H 3525 5550 50  0000 L CNN
F 1 "0.1u" H 3525 5350 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 3500 5450 50  0001 C CNN
F 3 "http://www.vishay.com/docs/45171/kseries.pdf" H 3500 5450 50  0001 C CNN
F 4 "BC2665CT-ND" H 3500 5450 60  0001 C CNN "Vendor_PN"
F 5 ".18" H 3500 5450 60  0001 C CNN "Price"
F 6 "Digi-Key" H 3500 5450 60  0001 C CNN "Vendor"
	1    3500 5450
	1    0    0    -1  
$EndComp
$Comp
L C C4
U 1 1 5A406D42
P 4600 5500
F 0 "C4" H 4625 5600 50  0000 L CNN
F 1 "0.1u" H 4625 5400 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 4600 5500 50  0001 C CNN
F 3 "http://www.vishay.com/docs/45171/kseries.pdf" H 4600 5500 50  0001 C CNN
F 4 "BC2665CT-ND" H 4600 5500 60  0001 C CNN "Vendor_PN"
F 5 ".18" H 4600 5500 60  0001 C CNN "Price"
F 6 "Digi-Key" H 4600 5500 60  0001 C CNN "Vendor"
	1    4600 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 3800 2900 5200
Wire Wire Line
	2900 5200 3800 5200
Connection ~ 2900 3800
Wire Wire Line
	3500 5200 3500 5300
Connection ~ 3500 5200
Wire Wire Line
	4400 5200 5050 5200
Wire Wire Line
	4600 5200 4600 5350
$Comp
L GNDREF #PWR04
U 1 1 5A406EAF
P 4100 5950
F 0 "#PWR04" H 4100 5700 50  0001 C CNN
F 1 "GNDREF" H 4100 5800 50  0000 C CNN
F 2 "" H 4100 5950 50  0001 C CNN
F 3 "" H 4100 5950 50  0001 C CNN
	1    4100 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 5500 4100 5950
Wire Wire Line
	4600 5800 4600 5650
Wire Wire Line
	3500 5800 4600 5800
Connection ~ 4100 5800
Wire Wire Line
	3500 5800 3500 5600
$Comp
L Relay_DPDT SW1
U 1 1 5A40702F
P 6700 4650
F 0 "SW1" H 6700 4950 50  0000 C CNN
F 1 "Relay_DPDT" H 6700 4350 50  0001 C CNN
F 2 "dip_relays:Relay_DPDT_DIP16" H 6700 4650 50  0001 C CNN
F 3 "http://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=108-98007&DocType=SS&DocLang=EN" H 6700 4650 50  0001 C CNN
F 4 "Digi-Key" H 6700 4650 60  0001 C CNN "Vendor"
F 5 "PB383-ND" H 6700 4650 60  0001 C CNN "Vendor_PN"
F 6 "1.86" H 6700 4650 60  0001 C CNN "Price"
	1    6700 4650
	1    0    0    -1  
$EndComp
$Comp
L Relay_DPDT SW1
U 2 1 5A4070D8
P 5300 5200
F 0 "SW1" H 5100 5100 50  0000 C CNN
F 1 "Relay_DPDT" H 5300 4900 50  0001 C CNN
F 2 "" H 5300 5200 50  0001 C CNN
F 3 "" H 5300 5200 50  0001 C CNN
	2    5300 5200
	1    0    0    -1  
$EndComp
$Comp
L Relay_DPDT SW1
U 3 1 5A40734A
P 6550 3800
F 0 "SW1" H 6450 3950 50  0000 C CNN
F 1 "Relay_DPDT" H 6550 3500 50  0001 C CNN
F 2 "" H 6550 3800 50  0001 C CNN
F 3 "" H 6550 3800 50  0001 C CNN
	3    6550 3800
	1    0    0    -1  
$EndComp
Connection ~ 4600 5200
Wire Wire Line
	5550 5300 7050 5300
Wire Wire Line
	7050 5300 7050 4000
Wire Wire Line
	7050 4000 8500 4000
$Comp
L R R2
U 1 1 5A40751C
P 7750 4300
F 0 "R2" V 7830 4300 50  0000 C CNN
F 1 "1k" V 7650 4300 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7680 4300 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-CF_CFM.pdf" H 7750 4300 50  0001 C CNN
F 4 "CF14JT1K00CT-ND" V 7750 4300 60  0001 C CNN "Vendor_PN"
F 5 ".10" V 7750 4300 60  0001 C CNN "Price"
F 6 "Digi-Key" V 7750 4300 60  0001 C CNN "Vendor"
	1    7750 4300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7900 4300 8050 4300
Connection ~ 8050 4300
Wire Wire Line
	7600 4300 7550 4300
Wire Wire Line
	7550 4300 7550 4000
Connection ~ 7550 4000
Wire Wire Line
	8500 3400 4850 3400
Wire Wire Line
	4850 3400 4850 5200
Connection ~ 4850 5200
Wire Wire Line
	8050 3400 8050 3000
Connection ~ 8050 3400
$Comp
L GNDREF #PWR05
U 1 1 5A4F1491
P 5200 4550
F 0 "#PWR05" H 5200 4300 50  0001 C CNN
F 1 "GNDREF" H 5200 4400 50  0000 C CNN
F 2 "" H 5200 4550 50  0001 C CNN
F 3 "" H 5200 4550 50  0001 C CNN
	1    5200 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 4300 4450 4300
$EndSCHEMATC
