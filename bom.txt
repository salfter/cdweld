Qty	Reference(s)	Value	LibPart	Footprint	Datasheet	Price	Vendor	Vendor_PN	ext price
1	C1	10u	proper-passives:CP	Capacitors_ThroughHole:CP_Radial_D5.0mm_P2.00mm	https://search.kemet.com/component-edge/download/datasheet/ESK106M035AC3AA.pdf	0.15	Digi-Key	399-6598-ND	0.15
4	C2, C3, C4, C5	0.1u	proper-passives:C	Capacitors_ThroughHole:C_Disc_D5.0mm_W2.5mm_P5.00mm	http://www.vishay.com/docs/45171/kseries.pdf	0.18	Digi-Key	BC2665CT-ND	0.72
1	D1	1N4001	device:D_ALT	Diodes_ThroughHole:D_DO-41_SOD81_P10.16mm_Horizontal	http://www.mccsemi.com/up_pdf/1N4001-1N4007(DO-41).pdf	0.11	Digi-Key	1N4001-TPMSCT-ND	0.11
1	J1	24V In	conn:Barrel_Jack	Connect:Barrel_Jack_CUI_PJ-102AH	http://www.cui.com/product/resource/digikeypdf/pj-102ah.pdf	0.76	Digi-Key	CP-102AH-ND	0.76
1	J2	Conn_01x12	conn:Conn_01x12	Connectors_Phoenix:PhoenixContact_MC-G_12x3.50mm_Angled	https://media.digikey.com/pdf/Data%20Sheets/Phoenix%20Contact%20PDFs/1988901.pdf	2.07	Digi-Key	277-1774-ND	2.07
2	R1, R2	1k	proper-passives:R	Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal	https://www.seielect.com/Catalog/SEI-CF_CFM.pdf	0.1	Digi-Key	CF14JT1K00CT-ND	0.2
1	SW1	Relay_DPDT	proper-passives:Relay_DPDT	dip_relays:Relay_DPDT_DIP16	http://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=108-98007&DocType=SS&DocLang=EN	1.86	Digi-Key	PB383-ND	1.86
1	U1	L200C	regul:L200	TO_SOT_Packages_THT:TO-220-5_Vertical_StaggeredType1	http://www.st.com/content/ccc/resource/technical/document/datasheet/04/2c/9f/b5/65/15/49/ac/CD00000053.pdf/files/CD00000053.pdf/jcr:content/translations/en.CD00000053.pdf	3.29	Digi-Key	497-1382-5-ND	3.29
1	U2	L78L05_TO92	regul:L78L05_TO92	TO_SOT_Packages_THT:TO-92_Inline_Narrow_Oval	http://www.taiwansemi.com/products/datasheet/TS78L00_I15.pdf	0.44	Digi-Key	TS78L05CT A3GCT-ND	0.44
1		50RIA20	n/a	n/a	http://www.vishay.com/docs/93711/vs-50ria.pdf	15.5	Digi-Key	50RIA20-ND	15.5
1		10k	n/a	n/a	http://www.ttelectronics.com/sites/default/files/download-files/Datasheet_RotaryPanelPot_P160series.pdf	0.76	Digi-Key	987-1661-ND	0.76
1			n/a	n/a	https://media.digikey.com/pdf/Data%20Sheets/Aavid%20Thermal%20Technologies%20PDFs/581002B00000.pdf	1.31	Digi-Key	HS300-ND	1.31
